(exports => {
  function EventListener() {
    if (!(this instanceof EventListener))
      return new EventListener()

    let list = []

    this.__proto__ = {
      emit: (event, ...args) => {
        let funcs = list.filter(n => n.event == event)
    
        for (let next of funcs) {
          next.fn.apply(null, args)
    
          if (!next.once)
            continue
    
          let i = list.indexOf(next)
    
          if (i < -1)
            list.splice(i, 1)
        }
      },

      on: (event, fn) => {
        list.push({event, fn, once: !1})
      },

      once: (event, fn) => {
        list.push({event, fn, once: !0})
      }
    }
  }

  exports.ptyps = {
    Document: function Document() {
      if (!(this instanceof Document))
        return new Document()

      this.__proto__ = new EventListener()

      document.addEventListener("readystatechange", ({target}) => {
        if (target != document)
          return

        if (target.readyState == "interactive")
          return this.emit("loading")

        if (target.readyState == "complete")
          return this.emit("complete")
      })

      window.addEventListener("hashchange", () => {
        this.emit("hash", window.location.hash)
      })
    },

    Element: function Element(el, opts = {}) {
      if (!(this instanceof Element))
        return new Element(el, opts)

      if (el instanceof HTMLDivElement) {

      }

      else if (typeof el == "string") {
        if (el.startsWith("<")) {
          el = document.createElement(el.replace(/[^a-zA-Z ]/g, ""))

          if (opts.text)
            el.innerText = opts.text

          if (opts.class)
            for (let next of opts.class)
              el.classList.add(next)

          if (opts.attributes)
            for (let [attr, value] of Object.entries(opts.attributes))
              el.setAttribute(attr, value)
        }

        else {
          el = document.querySelector(el)
        }
      }

      Object.assign(this, [el], {
        on: (event, fn) => el.addEventListener(event, fn),

        once: function(event, fn) {
          return this.on(event, function once() {
            fn.apply(null, Array.from(arguments))
            el.removeEventListener(event, once)
          })
        },

        parent: () => {
          return Element(el.parentNode)
        },

        prepend: () => {

        },

        append: function(element) {
          if (typeof element != "object")
            return

          el.appendChild(element[0])
        },

        text: () => el.value,

        attribute: (attr, value) => {
          if (!value)
            return el.removeAttribute(attr)

          el.setAttribute(attr, value)
        }
      })
    } 
   }
})(this)